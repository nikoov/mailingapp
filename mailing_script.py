import django
import os
import requests
import json
from django.core.exceptions import ObjectDoesNotExist

import config

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mailingapp.settings")
django.setup()

import mailing.models
from mailing.models import Mailing, Recipient, Message
from datetime import datetime, time

def sendMessage(id, phone, text):
    URL = f'https://probe.fbrq.cloud/v1/send/{id}'

    headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + config.JWT_TOKEN,
        'Content-Type': 'application/json'
    }

    params = {
        'id': int(id),
        'phone': int(phone),
        'text': text,
    }
    try:
        r = requests.post(URL, data=json.dumps(params), headers=headers, timeout=2)
    except requests.exceptions.ConnectionError as e:
        return {'code': 503, 'message': e}


    if r.status_code == 200:
        return r.json()
    else:
        print(r.status_code)
        return {'code': r.status_code}

#Message.objects.all().delete()

def main():
    while True:
        for m in Mailing.objects.all():
            tel_code, tag = m.filter_field.split(' ')
            t = time(0, 0)
            start_date = datetime.combine(m.start_date, t).timestamp()
            end_date = datetime.combine(m.end_date, t).timestamp()
            now_date = datetime.now().timestamp()

            if start_date < now_date and end_date > now_date:
                recipients = Recipient.objects.filter(tel_code=tel_code, tag=tag)

                for r in recipients:
                    try:
                        mes = Message.objects.get(mailing_id=Mailing.objects.get(id=m.id), recipient_id=Recipient.objects.get(id=r.id))
                    except ObjectDoesNotExist:
                        mes = Message.objects.create(sent_date=datetime.now().strftime('%Y-%m-%d'),
                                                     mailing_id=Mailing.objects.get(id=m.id),
                                                     recipient_id=Recipient.objects.get(id=r.id))
                    if mes.sent_status == False:
                        res = sendMessage(id=mes.id, phone=r.tel_num, text=m.message_text)
                        if res.get('message') == 'OK':
                            mes.sent_status = True
                            mes.sent_date = datetime.now().strftime('%Y-%m-%d')
                            print(f'Message to {r.tel_num} sent!')
                        else:
                            print(f'Error with sending message to {r.tel_num}')
                    mes.save()


if __name__ == '__main__':
    main()
    # url = 'http://127.0.0.1:8000/api/mailings/5/'
    #
    # data = {
    #     'message_text': 'prtt',
    #     'start_date': '2001-01-01',
    #     'end_date': '2001-01-02',
    #     'filter_field': '962 lohh'
    # }
    #
    # r = requests.put(url, data=json.dumps(data), headers={'Content-Type': 'application/json'})
    # print(r.status_code)

