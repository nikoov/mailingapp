"""mailingapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework.routers import SimpleRouter

from mailing.views import MailingView, RecipientView, MessageView, mailings_general_stat, mailings_detailed_stat

router = SimpleRouter()
router.register('api/mailings', MailingView, 'mailing')
router.register('api/recipients', RecipientView)
router.register('api/message', MessageView)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/mailings/stat/general/', mailings_general_stat),
    path('api/mailings/stat/detailed/<int:mailing_id>/', mailings_detailed_stat)
]

urlpatterns += router.urls
