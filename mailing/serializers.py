from rest_framework.serializers import ModelSerializer

from mailing.models import Mailing, Recipient, Message


class MailingSerializer(ModelSerializer):
    class Meta:
        model = Mailing
        fields = ['message_text', 'start_date', 'end_date', 'filter_field']

class RecipientSerializer(ModelSerializer):
    class Meta:
        model = Recipient
        fields = ['tel_num', 'tel_code', 'tag', 'time_zone']

class MessageSerializer(ModelSerializer):
    class Meta:
        model = Message
        fields = ['id', 'sent_date', 'sent_status', 'mailing_id', 'recipient_id']

