from django.db import models

class Mailing(models.Model):
    message_text = models.TextField()
    start_date = models.DateField()
    end_date = models.DateField()
    filter_field = models.CharField(max_length=50)

    def __str__(self):
        return str(self.id)

class Recipient(models.Model):
    tel_num = models.CharField(max_length=15)
    tel_code = models.CharField(max_length=3)
    tag = models.CharField(max_length=5)
    time_zone = models.CharField(max_length=10)

    def __str__(self):
        return str(self.id)

class Message(models.Model):
    sent_date = models.DateField()
    sent_status = models.BooleanField(default=False)
    mailing_id = models.ForeignKey(Mailing, on_delete=models.PROTECT)
    recipient_id = models.ForeignKey(Recipient, on_delete=models.PROTECT)

    def __str__(self):
        return str(self.id)