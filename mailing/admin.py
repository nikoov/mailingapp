from django.contrib import admin
from .models import Mailing, Message

class MailingsAdmin(admin.ModelAdmin):
    list_display = ('id', 'message_text', 'start_date', 'end_date', 'filter_field')
    list_display_links = ('id',)
    search_fields = ('id', 'filter_field')
    list_editable = ('message_text', 'start_date', 'end_date', 'filter_field')
    list_filter = ('start_date',)

class MessagesAdmin(admin.ModelAdmin):
    list_display = ('id', 'sent_date', 'sent_status', 'mailing_id', 'recipient_id')
    list_display_links = ('id',)
    search_fields = ('id', 'mailing_id')
    list_editable = ()
    list_filter = ('sent_date',)

admin.site.register(Mailing, MailingsAdmin)
admin.site.register(Message, MessagesAdmin)