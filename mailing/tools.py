def get_object_as_dict(model_, obj):
    r = {}
    for field in model_._meta.fields:
        field_object = model_._meta.get_field(field.name)
        field_value = field_object.value_from_object(obj)
        r.update({field.name: field_value})
    return r