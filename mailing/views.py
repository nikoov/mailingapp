import json

from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet

from mailing.serializers import MailingSerializer, RecipientSerializer, MessageSerializer
from .models import Mailing, Recipient, Message
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist
from .tools import get_object_as_dict

#TODO
# {
#     "message_text": "Привет",
#     "start_date": "2022-09-18",
#     "end_date": "2022-09-22",
#     "messages": {
#         'total': 24,
#         'sent': 12,
#         'unsent': 12,
#     }
# }


@api_view(['GET'])
def mailings_general_stat(request):
    if request.method == 'GET':
        res = []
        mailings = Mailing.objects.all()
        for m in mailings:
            messages = Message.objects.filter(mailing_id=m.id)
            msgs_total = messages.count()
            msgs_sent = Message.objects.filter(mailing_id=m.id, sent_status=True).count()
            msgs_unsent = msgs_total - msgs_sent
            r = {}
            for field in Mailing._meta.fields:
                field_object = Mailing._meta.get_field(field.name)
                field_value = field_object.value_from_object(m)
                r.update({field.name: field_value})
            r.update(
                {
                    'messages': {
                        'total': msgs_total,
                        'sent': msgs_sent,
                        'unsent': msgs_unsent
                    }
                }
            )
            res.append(r)

        return Response(res)


@api_view(['GET'])
def mailings_detailed_stat(request, mailing_id):
    if request.method == 'GET':
        res = []
        try:
            mailing = Mailing.objects.get(id=mailing_id)
        except ObjectDoesNotExist:
            return Response({'code': 404, 'message': "mailing doesn't exist"})

        messages = Message.objects.filter(mailing_id=mailing_id)
        msgs_total = messages.count()
        msgs_sent = Message.objects.filter(mailing_id=mailing.id, sent_status=True).count()
        msgs_unsent = msgs_total - msgs_sent

        r = get_object_as_dict(Mailing, mailing)
        r.update(
            {
                'messages': {
                    'total': msgs_total,
                    'sent': msgs_sent,
                    'unsent': msgs_unsent,
                    'data': []
                }
            }
        )

        for mes in messages:
            mes_d = get_object_as_dict(Message, mes)
            r['messages']['data'].append(mes_d)

        return Response(r)


class MailingView(ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

class RecipientView(ModelViewSet):
    queryset = Recipient.objects.all()
    serializer_class = RecipientSerializer

class MessageView(ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer